import logging
import csv
from data_parser import JSONParser


class FileHandler:
    def __init__(self, input_path):
        self.input_path = input_path

    def read_input_file(self):
        try:
            with open(self.input_path, 'r') as reader:
                parser = JSONParser(reader)
                return parser.parse_json_file()
        except FileNotFoundError:
            logging.error(f'Input file does not exists at {self.input_path} - Please provide a valid path.')

    def write_output_file(self, output_path, output_data):
        try:
            with open(output_path, 'w') as file_writer:
                keys = list(output_data[0].keys())
                csv_writer = csv.DictWriter(file_writer, keys)
                csv_writer.writeheader()
                csv_writer.writerows(output_data)
        except FileExistsError:
            logging.error(f'Another file is located at {output_path} - Please provide a valid path.')


