# Car Data Analyzer Project

This is a project made for evaluation purposes

## Analysis of the problem

### Data Set
A data set with information related to cars manufactured in United States.

### Data Format
The format of the input file is a traditional file with *.json extension

### Data Structure
The data is structured based in the following fields:
1) id: sequential ID
2) Manufacturer: name of car manufacturer
3) Model: model name  of the car
4) VIN: car's unique identification number
5) Year: year manufactured in
6) Color: color of the car  

We don't have any nested data, but the field color is not always present. 

### Dealing with the absence of color field
To deal with the fact of absence color field I just included **None** type in python code to preserve some data consistency. This was the only step done in the *Data Cleaning* step.

### Solutions
When I was analyzing the problem I used two approaches.
The first one was taking the content of the file bu chunks and process the data item by item. I think it is the best approach for large data sets but we have for this problem a file with only 1000 elements to be analyzed. For simplicity, range of the data and only use *json.load* method to read the file I jumped to the next approach.

This approach was getting all the data with *json.load* method. Get all this set into memory and analyze it execution time. This solution is not the best for large data sets but it is working fine with the dimension of the problem. So I picked up this one.

### Separation of Concerns
I separated the solution in the following modules:
1) business: this module contains all the classes related with the business model of the data
2) config: we can find here all the configuration required for the project.
3) data_parser:  we have here all the logic related with parsing our files to json and csv formats.
4) file_handler: this module contains the file handling operations
5) data_processor: this is the main module. It uses the operations provided by the rest of the modules to run the operations required for the solution

## Running the project
### Requirements
In order to run the project properly, I recommend to install the following tools:
- Python 3.7.3+
- pip

For this solution, I didn't use any external dependency but for good practices I used **venv** to manage the basic required dependencies.
To run the project you just need to use the following instructions: 
```bash
cd car_data_analyzer
pip install -r requirements.txt
python data_processor.py
```
Remember put the input_file, first_output_file and second_output_file parameters in the configuration file before running the project.