import json
from business import Car, CarInventory


def json_to_car(json_dict):
    return Car(json_dict['id'],
               json_dict['Manufacturer'],
               json_dict['Model'],
               json_dict['VIN'],
               json_dict['year'],
               json_dict['color'] if 'color' in json_dict.keys() else None)


class JSONParser:
    def __init__(self, input_json_file):
        self.input_json_file = input_json_file

    def parse_json_file(self):
        values = map(json_to_car, tuple(json.load(self.input_json_file)))
        inventory = CarInventory()
        for value in values:
            inventory.add_car(value)
        return inventory

