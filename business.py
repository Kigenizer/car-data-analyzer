class Car:
    def __init__(self, id, manufacturer, model, vin, year, color):
        self.id = id
        self.manufacturer = manufacturer
        self.model = model
        self.vin = vin
        self.year = year
        self.color = color


class CarInventory:
    def __init__(self):
        self.car_list = []
        self.manufacturer_set = set()
        self.color_set = set()
        self.year_set = set()
        self.models_produced_per_year = []
        self.most_popular_color_per_year = []

    def add_car(self, car):
        self.car_list.append(car)
        self.manufacturer_set.add(car.manufacturer)
        if car.color is not None:
            self.color_set.add(car.color)
        self.year_set.add(car.year)


class CarInventoryCalculator:
    def __init__(self, car_inventory):
        self.car_inventory = car_inventory

    def init_first_resut(self):
        result = []
        for manufacturer in self.car_inventory.manufacturer_set:
            for year in self.car_inventory.year_set:
                result.append({'manufacturer': manufacturer,
                               'year': year,
                               'quantity': 0})
        self.car_inventory.models_produced_per_year = result

    def init_second_result(self):
        result = []
        for color in self.car_inventory.color_set:
            for year in self.car_inventory.year_set:
                result.append({'color': color,
                               'year': year,
                               'quantity': 0})
        self.car_inventory.most_popular_color_per_year = result

    def calculate_models_produced_per_car(self, manufacturer, year):
        for item in self.car_inventory.models_produced_per_year:
            if item['manufacturer'] == manufacturer and item['year'] == year:
                item['quantity'] += 1

    def calculate_models_produced_per_year(self):
        for car in self.car_inventory.car_list:
            self.calculate_models_produced_per_car(car.manufacturer, car.year)
        return self.car_inventory.models_produced_per_year

    def calculate_colors_per_car(self, color, year):
        for item in self.car_inventory.most_popular_color_per_year:
            if item['color'] == color and item['year'] == year:
                item['quantity'] += 1

    def calculate_colors_per_year(self):
        for car in self.car_inventory.car_list:
            self.calculate_colors_per_car(car.color, car.year)

    def calculate_most_used_color_given_year(self, year):
        colors_per_year = list(filter(lambda x: x['year'] == year, self.car_inventory.most_popular_color_per_year))
        return max(colors_per_year, key=lambda x: x['quantity'])

    def calculate_most_used_color_results(self):
        result = []
        for year in self.car_inventory.year_set:
            result.append(self.calculate_most_used_color_given_year(year))
        return result
