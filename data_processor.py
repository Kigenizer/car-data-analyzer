import config
from file_handler import FileHandler
from business import CarInventoryCalculator

handler = FileHandler(config.IO_CONFIG['input_file'])


def calculate_global_results():
    input_data = handler.read_input_file()
    inventory_calculator = CarInventoryCalculator(input_data)
    inventory_calculator.init_first_resut()
    inventory_calculator.init_second_result()
    first_result = inventory_calculator.calculate_models_produced_per_year()
    inventory_calculator.calculate_colors_per_year()
    second_result = inventory_calculator.calculate_most_used_color_results()
    return first_result, second_result


def write_results(result_one, result_two):
    handler.write_output_file(config.IO_CONFIG['first_output_file'], result_one)
    handler.write_output_file(config.IO_CONFIG['second_output_file'], result_two)


if __name__ == '__main__':
    result_one, result_two = calculate_global_results()
    write_results(result_one, result_two)
